# Information

This project is currently being worked on by Alex F, Josh P, Alex A, and Daryus M. This is a project for a two part, multi-user chat client. 



### Resources

Some resources that were used to help understand some libraries and other concepts.


* [JSON](https://realpython.com/python-json/)
* [Socket](https://docs.python.org/3/howto/sockets.html)
* [More Socket](https://www.geeksforgeeks.org/socket-programming-python/)
* [Threading](https://realpython.com/intro-to-python-threading/)
* [Sqlite3](https://www.digitalocean.com/community/tutorials/how-to-use-the-sqlite3-module-in-python-3)
* [Curses](https://www.devdungeon.com/content/curses-programming-python)
* [RSA](https://simple.wikipedia.org/wiki/RSA_algorithm)
* [Miller-Rabin](https://en.wikipedia.org/wiki/Miller%E2%80%93Rabin_primality_test)
* [Email](https://realpython.com/python-send-email/)
