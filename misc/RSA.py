import math
import random

def miller_rabin(n, k):
    if n % 5 ==0:
        return False
    r, s = 0, n - 1
    while s % 2 == 0:
        r += 1
        s //= 2
    for _ in range(k):
        a = random.randrange(2, n - 1)
        x = pow(a, s, n)
        if x == 1 or x == n - 1:
            continue
        for _ in range(r - 1):
            x = pow(x, 2, n)
            if x == n - 1:
                break
        else:
            return False
    return True

def gen_prime(bit_size):
    number = random.randrange(pow(2,bit_size)+1,pow(2,bit_size+1)-1,2)
    while True:
        if miller_rabin(number, 40) == True:
            return number
            break
        number = number + 2

def gen_key(prime_1 = True, prime_2 = True, key_size = 16):
    if prime_1 == True:
        p = gen_prime(key_size)
    else:
        p = prime_1
    if prime_2 == True:
        q = gen_prime(key_size)
    else:
        q = prime_2
    N = p * q
    r = (p - 1) * (q - 1)
    candidate_integer = 1
    while True:
        candidate = candidate_integer * r + 1
        n = candidate
        e = 0
        d = 0
        for prime in [3,     7,  11,  13,  17,  19,  23,  29,
                      31,   37,  41,  43,  47,  53,  59,  61,
                      67,   71,  73,  79,  83,  89,  97, 101,
                      103, 107, 109, 113, 127, 131, 137, 139,
                      149, 151, 157, 163, 167, 173, 179, 181,
                      191, 193, 197, 199, 211, 223, 227, 229,
                      233, 239, 241, 251, 257, 263, 269, 271,
                      277, 281, 283, 293, 307, 311, 313, 317, ]:
            if n % prime == 0:
                e = prime
                d = n // e
        if d != 0:
            break
        candidate_integer = candidate_integer + 1
    return {"pub_key":{"e":e,"N":N},"priv_key":{"e":e,"d":d,"N":N,"p":p,"q":q,"r":r}}

def check_key(key):
    if key["pub_key"]["e"] != key["priv_key"]["e"]:
        return False
    checked_num = random.randint(100, pow(10, 10))
    encrypted_num = pow(checked_num, key["pub_key"]["e"], key["pub_key"]["N"])
    decrypted_num = pow(encrypted_num, key["priv_key"]["d"], key["priv_key"]["N"])
    if decrypted_num == checked_num:
        return True
    else:
        return False

def str_to_int_list(s: str):
    return [ord(x) for x in list(s)]

def int_list_to_str(int_list: list):
    return_string = ""
    for c in int_list:
        return_string = f"{return_string}{chr(c)}"
    return(return_string)

def str_to_int(s: str):
    string_bytes = bytes(s, "utf8")
    return(int.from_bytes(string_bytes, "big", signed=False))

def int_to_str(num: int):
    num_length = max(1, math.ceil(num.bit_length() / 8))
    return(num.to_bytes(num_length, "big").decode("utf8"))

def encrypt(pub_key, message, return_as_bytes = True, byte_order = "big"):
    message_as_bytes = bytes(message, "utf8")
    message_as_number = int.from_bytes(message_as_bytes, byte_order)
    encrypted_message = pow(message_as_number, pub_key["e"], pub_key["N"])
    if return_as_bytes == True:
        return encrypted_message.to_bytes(int(math.log(encrypted_message, 256))+1, byte_order)
    else:
        return encrypted_message

def decrypt(priv_key, message, is_bytes = True, byte_order = "big"):
    if is_bytes == True:
        message_as_number = int.from_bytes(message, byte_order)
    else:
        message_as_number = message
    message_number_decrypted = pow(message_as_number, priv_key["d"], priv_key["N"])
    return message_number_decrypted.to_bytes(int(math.log(message_number_decrypted, 256))+1, byte_order).decode()
