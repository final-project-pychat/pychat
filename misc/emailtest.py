import smtplib
import random
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

msg = MIMEMultipart()
msg['From'] = "pychatmailer@gmail.com"
password = input("Enter password to pychatmailer@gmail.com: ")
msg['To'] = input("Enter email to send code to: ")
msg['Subject'] = "Verification Code"

#numbers
alphabets_in_capital=[]
for i in range(48,57):
    alphabets_in_capital.append(chr(i))
#upercase
for i in range(65,90):
    alphabets_in_capital.append(chr(i))
#lowercase
for i in range(97,122):
    alphabets_in_capital.append(chr(i))

#print(alphabets_in_capital)

code = ""

#pick random characters and add it to code 8 times
for i in range(0,8):
    code = code + str(random.choice(alphabets_in_capital))


#Email Body Content
msg.attach(MIMEText("This is your code: " + code, 'plain'))
#Establish SMTP Connection
s = smtplib.SMTP('smtp.gmail.com: 587')
#Start TLS based SMTP Session
s.starttls()
#Login Using Your Email ID & Password
s.login(msg['From'], password)

#To Send the Email
s.sendmail(msg['From'], msg['To'], msg.as_string())
#ask for answer to code
user_answer = str(input("Please enter the code sent in your email! Enter r to have it sent again or press q to exit. "))

while True:
    if user_answer == "r":
        s.sendmail(msg['From'], msg['To'], msg.as_string())
    elif user_answer == "q":
        break
    elif user_answer == code:
        print("Congratulations! You have successfully created an account.")
        break
    user_answer = str(input("Please enter the code sent in your email! Enter r to have it sent again or press q to exit. "))


#Terminating the SMTP Session
s.quit()
