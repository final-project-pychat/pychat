""" This code has not been tested yet. This code should work. Code has been designed to be directly put into the main server code."""


# Importing libraries
import sqlite3
import socket
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

# Main Class
class Login_Process:
    email_password = input("Enter password to pychatmailer@gmail.com: ")
    def __init__(self, conn, addr):
        # Key variables
        self.code = ""
        self.HasAcc = ""
        self.Uname = ""
        self.Password = ""
        self.Email = ""
        self.user_answer = ""
        self.msg = MIMEMultipart()
        self.msg['From'] = "pychatmailer@gmail.com"
        self.msg['Subject'] = "Verification Code"
        self.s = smtplib.SMTP('smtp.gmail.com: 587')
        self.s.starttls()
        self.s.login(self.msg['From'], Login_Process.password)
        self.connection = sqlite3.connect('userdatabase.db')
        self.cursor = self.connection.cursor()
        self.conn = conn
        self.addr = addr
        try:
            self.connection.execute("SELECT Username, Password, Email from Users")
        except:
            self.connection.execute('CREATE TABLE "Users" ("Username" TEXT, "Password" TEXT, "Email" TEXT)')
        self.run()

    def run(self):
        self.welcome = {"message" : "Welcome to Pychat! Please log into the server."}
        self.send_welcome = json.dumps(welcome)
        self.server_socket.send(self.send_welcome)
        self.HasAcc_que = {"message": "Do you already have an account? Enter exactly Y or N. "}
        self.send_HasAcc = json.dumps(self.HasAcc_que)
        self.server_socket.send(self.send_HasAcc)
        while self.HasAcc != "Y" and self.HasAcc != "N":
            self.data = self.conn.recv(1024).decode("utf8")
            if not self.data:
                break
            self.data_as_dictionary = json.loads(self.data)
            if set(self.data_as_dictionary) != {"response"}:
                break
            self.HasAcc = self.data_as_dictionary["response"]
            self.server_socket.send(self.send_HasAcc)


        if self.HasAcc == "N":
            self.make_account()

        self.login_server()
        self.end()

    def email_verification(self):
        # Variable
        self.alphabets_in_capital = []

        # Numbers
        for i in range(48,57):
            self.alphabets_in_capital.append(chr(i))
        # Upercase
        for i in range(65,90):
            self.alphabets_in_capital.append(chr(i))
        # Lowercase
        for i in range(97,122):
            self.alphabets_in_capital.append(chr(i))
        # Picks 8 random numbers/letters
        for i in range(0,8):
            self.code = self.code + str(random.choice(self.alphabets_in_capital))

        # Email's message
        self.msg.attach(MIMEText("This is your code: " + self.code, 'plain'))
        # Sends email
        self.s.sendmail(self.msg['From'], self.msg['To'], self.msg.as_string())

    # Function to create a new account
    def make_account(self):
        # Gets how big the table is
        self.cursor.execute("SELECT count(*) FROM Users")
        self.table_size = self.cursor.fetchall()

        self.creating = True
        self.passmaking = True

        if self.HasAcc == "N":
            while self.creating == True:
                self.data = self.conn.recv(1024).decode("utf8")
                if not self.data:
                    break
                self.data_as_dictionary = json.loads(self.data)
                if set(self.data_as_dictionary) != {'username', 'password', 'email'}:
                    break

                self.Uname = self.data_as_dictionary["username"]
                self.msg['To'] = self.data_as_dictionary["email"]
                self.Email = self.data_as_dictionary["email"]

                if self.table_size[0][0] == 0:
                    break
                else:
                    self.cursor = self.connection.execute("SELECT Username, Password, Email from Users")

                    # Checks if the username or email is already being used
                    for row in self.cursor:
                        if self.Uname == row[0] or self.Email == row[2]:
                            self.taken_msg = {"message": f"The username {self.Uname} is already taken or the email {self.Email} is already in use.", "status": True}
                            self.send_this = json.dumps(self.taken_msg)
                            self.server_socket.send(self.send_this)
                        else:
                            self.creating = False

            while self.passmaking == True:
                self.data = self.conn.recv(1024).decode("utf8")
                if not self.data:
                    break
                self.data_as_dictionary = json.loads(self.data)
                if set(self.data_as_dictionary) != {'username', 'password', 'email'}:
                    break
                self.Password = self.data_as_dictionary["password"]
                if self.Password != "":
                    self.passmaking = False

            self.email_verification()
            # Not sure how you would do this. The goal is to send this message to the client and then the client asks the user to answer
            self.user_answer = {"message": "Please enter the code sent to your email! Enter r to have it sent again.")
            self.send_user_answer = json.dumps(self.user_answer)
            self.server_socket.send(self.send_user_answer)
            while True:
                self.data = self.conn.recv(1024).decode("utf8")
                if not self.data:
                    break
                self.data_as_dictionary = json.loads(self.data)
                if set(self.data_as_dictionary) != {'message'}:
                    break
                self.answer = self.data_as_dictionary["message"]

                if self.answer == "r":
                    self.s.sendmail(self.msg['From'], self.msg['To'], self.msg.as_string())
                elif self.answer == self.code:
                    self.done = {"message": "Congratulations! You have successfully created an account."}
                    self.send_done = json.dumps(self.done)
                    self.server_socket.send(self.send_done)
                    self.s.quit()
                    break
                self.server_socket.send(self.send_user_answer)

            self.cursor.execute('INSERT INTO Users (Username, Password, Email) VALUES (?, ?, ?)', (self.Uname, self.Password, self.Email))
            self.connection.commit()
            self.HasAcc = "Y"
            self.reload()

    # "Reloads" the database so that the new account is shown
    def reload(self):
        self.cursor = self.connection.execute("SELECT Username, Password, Email from Users")

    def login_server(self):
        self.Signedin = False

        if self.HasAcc == "Y":
            while self.Signedin == False:
                self.data = self.conn.recv(1024).decode("utf8")
                if not self.data:
                    break
                self.data_as_dictionary = json.loads(self.data)
                if set(self.data_as_dictionary) != {'username', 'password', 'email'}:
                    break
                self.Uname = self.data_as_dictionary["username"]
                self.Email = self.data_as_dictionary["email"]
                self.Password = self.data_as_dictionary["password"]
                self.accexist = False

                for row in self.cursor:
                    if (self.Uname, self.Password) == (row[0],row[1]) or (self.Email, self.Password) == (row[2], row[1]):
                        self.Signedin = True
                        pass
                    if self.Uname == row[0] or self.Email == row[2]:
                        self.accexist = True

            if self.accexist != True:
                self.none = {"message": f"There is no user/email named {self.Uname_Email} in this server."}
                self.send_none = json.dumps(self.none)
                self.server_socket.send(self.send_none)
            if self.Signedin == False:
                self.fail = {"message": "Login failed. Please try again."}
                self.send_fail = json.dumps(self.fail)
                self.server_socket.send(self.send_fail)

            self.cursor = self.connection.execute("SELECT Username, Password, Email from Users")

    # Closes the connection to the database
    def end(self):
        self.connection.close()
        self.success = {"message": "Successfully Logged In!"}
        self.send_success = json.dumps(self.success)
        self.server_socket.send(self.send_success)

# Makes an instance of the main class
""" Call this in NewConnection() """
run = Login_Process()

