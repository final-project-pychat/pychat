# Importing libraries 
import sqlite3
import smtplib
import random
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

class Login_Process:
    def __init__(self):
        # Key variables
        self.password = input("Enter password to pychatmailer@gmail.com: ")
        self.code = ""
        self.HasAcc = ""
        self.Uname = ""
        self.Password = ""
        self.Email = ""
        self.user_answer = ""
        self.msg = MIMEMultipart()
        self.msg['From'] = "pychatmailer@gmail.com"
        self.msg['Subject'] = "Verification Code"
        self.s = smtplib.SMTP('smtp.gmail.com: 587')
        self.s.starttls()
        self.s.login(self.msg['From'], self.password)
        self.connection = sqlite3.connect('userdatabase.db')
        self.cursor = self.connection.cursor()
        try:
            self.connection.execute("SELECT Username, Password, Email from Users")
        except:
            self.connection.execute('CREATE TABLE "Users" ("Username" TEXT, "Password" TEXT, "Email" TEXT)')
        self.run()

    def run(self):
        print("Welcome to Pychat! Please log into the server.")
        # Asks if person has an account
        while self.HasAcc != "Y" and self.HasAcc != "N":
            self.HasAcc = input("Do you already have an account? Enter exactly Y or N. ")

        if self.HasAcc == "N":
            self.make_account()

        self.login_server()
        self.end()
    
    def email_verification(self):
        # Variable
        self.alphabets_in_capital = []

        # Numbers
        for i in range(48,57):
            self.alphabets_in_capital.append(chr(i))
        # Upercase
        for i in range(65,90):
            self.alphabets_in_capital.append(chr(i))
        # Lowercase
        for i in range(97,122):
            self.alphabets_in_capital.append(chr(i))
        # Picks 8 random numbers/letters 
        for i in range(0,8):
            self.code = self.code + str(random.choice(self.alphabets_in_capital))
        
        # Email's message
        self.msg.attach(MIMEText("This is your code: " + self.code, 'plain'))
        # Sends email
        self.s.sendmail(self.msg['From'], self.msg['To'], self.msg.as_string())

    def make_account(self):
        # Gets how big the table is
        self.cursor.execute("SELECT count(*) FROM Users")
        self.table_size = self.cursor.fetchall()

        self.creating = True

        if self.HasAcc == "N":
            while self.creating == True:
                self.Uname = str(input("Enter your username. It will be space sensitive. "))
                self.msg['To'] = str(input("Enter your email. This email must be valid. "))
                self.Email = self.msg['To']

                # If the table is empty
                if self.table_size[0][0] == 0:
                    break
                else:
                    self.cursor = self.connection.execute("SELECT Username, Password, Email from Users")
                    
                    # Checks if the username or email is already being used
                    for row in self.cursor:
                        if self.Uname == row[0] or self.Email == row[2]:
                            print(f"The username {self.Uname} or the email {self.Email} is already taken.")
                        else:
                            self.creating = False
            self.Password = str(input("Enter your password. It will be space sensitive. "))
            self.email_verification()
            self.user_answer = str(input("Please enter the code sent to your email! Enter r to have it sent again. "))
            
            # Keeps the user in a loop until they enter the correct code
            while True:
                if self.user_answer == "r":
                    self.s.sendmail(self.msg['From'], self.msg['To'], self.msg.as_string())
                elif self.user_answer == self.code:
                    print("Congratulations! You have successfully created an account.")
                    self.s.quit()
                    break
                self.user_answer = str(input("Please enter the code sent in your email! Enter r to have it sent again. "))
            
            # Adds the newly created account to the database
            self.cursor.execute('INSERT INTO Users (Username, Password, Email) VALUES (?, ?, ?)', (self.Uname, self.Password, self.Email))
            self.connection.commit()
            self.HasAcc = "Y"
            self.reload()

    # "Reloads" the database so that the new account is shown
    def reload(self):
        self.cursor = self.connection.execute("SELECT Username, Password, Email from Users")

    def login_server(self):
        self.Signedin = False
        # Needed incase the user already has an account
        self.reload()

        if self.HasAcc == "Y":
            while self.Signedin == False:
                self.Uname_Email = str(input("What is your username or email? This is space sensitive. "))
                self.Password = str(input("What is your password? This is also space sensitive. "))
                self.accexist = False
                
                # Checks to see if the users account exists and if everything is correct
                for row in self.cursor:
                    if (self.Uname_Email, self.Password) == (row[0],row[1]) or (self.Uname_Email, self.Password) == (row[2], row[1]):
                        self.Signedin = True
                        pass
                    if self.Uname_Email == row[0] or self.Uname_Email == row[2]:
                        self.accexist = True

            if self.accexist != True:
                print(f"There is no user/email named {self.Uname_Email} in this server.")
            if self.Signedin == False:
                print("Login failed. Please try again.")

            self.cursor = self.connection.execute("SELECT Username, Password, Email from Users")

    # Closes the connection to the database
    def end(self):
        self.connection.close()
        print("Successfully Logged In!")

# Makes an instance of the main class
run = Login_Process()

