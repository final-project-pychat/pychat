import random
import time
import math

def prime(num):
        for x in range(3,int(math.sqrt(num)+1),2):
            if num % x == 0:
                return False
        return True

def miller_rabin(n, k):
    if n % 5 ==0:
        return False

    r, s = 0, n - 1
    while s % 2 == 0:
        r += 1
        s //= 2
    for _ in range(k):
        a = random.randrange(2, n - 1)
        x = pow(a, s, n)
        if x == 1 or x == n - 1:
            continue
        for _ in range(r - 1):
            x = pow(x, 2, n)
            if x == n - 1:
                break
        else:
            return False
    return True

#print(prime(int(input("Num:"))))
number = random.randrange(pow(2,32)+1,pow(2,33)-1,2)
while True:
    if miller_rabin(number, 40) == True:
        print(f"Found:{number}")
        break
    number = number + 2
