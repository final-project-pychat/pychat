from threading import Thread
import time


class Function:
    def __init__(self,name,phrase):
        self.name = name
        self.phrase = phrase
    def main(self):
        print(f"{self.name} started! The phrase is {self.phrase}.")
        time.sleep(1)
        print(f"{self.name} finished!")


function1 = Function("my thread","test phrase")
thread = Thread(target = function1.main)
thread.start()

function2 = Function("my other thread","test phrase2")
thread = Thread(target = function2.main)
thread.start()
