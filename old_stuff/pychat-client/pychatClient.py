#socket setup
import socket
#####################################################
#setting up ctrl+c
import signal
import sys
def signalHandler(sig, frame):
    curses.endwin()
    globals.kill=True
    sys.exit()
signal.signal(signal.SIGINT, signalHandler)
#####################################################
#threading setup
import threading
#####################################################
#Curses setup
import curses
class globals:
    kill=False
class winData:
    host=""
    port=""
    users=""
    username=""
stdscr=curses.initscr()
curses.start_color()
curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_GREEN)
curses.init_pair(2, curses.COLOR_BLACK, curses.COLOR_YELLOW)
#curses.init_pair(3, curses.COLOR_CYAN, curses.COLOR_BLACK)
curses.init_pair(3, 17, 8)
inputWin=curses.newwin(2,0,curses.LINES-2,0)
textWin=curses.newwin(curses.LINES-3,0,1,0)
infoWin=curses.newwin(1,0,0,0)
textWin.bkgd(' ', curses.color_pair(1))
inputWin.bkgd(' ', curses.color_pair(2))
infoWin.bkgd(' ', curses.color_pair(3))
textWin.scrollok(True)
#function to ask for input
def rawInput(stdscr, r, c, prompt_string):
    curses.echo()
    stdscr.addstr(r, c, prompt_string)
    stdscr.refresh()
    input = stdscr.getstr(r + 1, c)
    input = str(input)[2:-1]
    return input
#function to print from the bottom of the screen and scroll
def winMsg(stdscr,string,scrollAmount,row,col):
    stdscr.scroll(scrollAmount)
    stdscr.addstr(row,col,string)
    stdscr.refresh()
#updates the information displayed in the top window
def updateInfoWin():
    infoWin.clear()
    if curses.COLS >= len("Server:"+str(winData.host)+":"+str(winData.port)+"     "+str(winData.username)+"     "+str(winData.users)+" connected"):
        infoWin.addstr(0,0,"Server:"+str(winData.host)+":"+str(winData.port)+"     "+str(winData.username)+"     "+str(winData.users)+" connected")
    else:
        infoWin.addstr(0,0,"Please increase console width")
    infoWin.refresh()
#####################################################
#startup
#asking user for name, server, and port

inputWin.refresh()
textWin.refresh()
infoWin.refresh()
winData.host = rawInput(inputWin, 0, 0, "Server:")
inputWin.clear()
winData.port = 1208 #the default port if this doesn't work the user is asked for a port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
winData.username = rawInput(inputWin, 0, 0, "Username:")
inputWin.clear()


#connecting to server
try:
    s.connect((winData.host, winData.port))
except:
    winData.port = int(rawInput(inputWin, 0, 0, "Port:"))
    s.connect((winData.host, winData.port))
ready = False
while ready == False:
    data = s.recv(1024)
    if str(data)[2] == "p":#checks if their is a password
        winMsg(textWin,"This server is password protected.",1,curses.LINES-4,0)
        inputWin.clear()
        msg=rawInput(inputWin, 0, 0, "Server Password:")
        s.sendall(bytes("p"+msg, 'utf-8'))
        data = s.recv(1024)
        if str(data)[2] == "C":#checks if the password was correct
            winMsg(textWin,"correct password",1,curses.LINES-4,0)
            ready = True
        elif str(data)[2] == "w":#checks if the password was incorrect
            winMsg(textWin,"incorrect password",1,curses.LINES-4,0)
            winMsg(textWin,"press any key to close the program",1,curses.LINES-4,0)
            inputWin.clear()
            inputWin.getch()
            curses.endwin()
            sys.exit()
    elif str(data)[2] == "q":
        winMsg(textWin,"You are not on the whitelist for this server. Contact the server administrator if you think this is an error.",1,curses.LINES-4,0)
        winMsg(textWin,"press any key to close the program",1,curses.LINES-4,0)
        inputWin.clear()
        inputWin.getch()
        curses.endwin()
        sys.exit()
    elif str(data)[2] == "Q":
        winMsg(textWin,"You are banned on this server. Contact the server administrator if you think this is an error.",1,curses.LINES-4,0)
        winMsg(textWin,"press any key to close the program",1,curses.LINES-4,0)
        inputWin.clear()
        inputWin.getch()
        curses.endwin()
        sys.exit()
    else:
        ready = True


winMsg(textWin,"Connected to "+winData.host+" on port "+str(winData.port),1,curses.LINES-4,0)
updateInfoWin()
s.sendall(bytes("u"+winData.username, 'utf-8'))
#####################################################
#Functions for threading
def msgOutput(): #handles all data from the server
    while True:
        try:
            data = s.recv(1024)
            if str(data)[2] == "m":#checks if data is a message
                winMsg(textWin,(str(data)[3:-1]),1,curses.LINES-4,0)
            elif str(data)[2] == "c" and winData.users != int(str((data))[3:-1]): #checks if data is updateing the user count
                winData.users = int(str((data))[3:-1])
                updateInfoWin()
            elif str(data)[2] == "q":
                winMsg(textWin,"press any key to close the program",1,curses.LINES-4,0)
                inputWin.clear()
                inputWin.getch()
                globals.kill = True
                curses.endwin()
            if globals.kill == True: #checks if the loop is being killed
                break
        except ValueError:
            pass

def msgInput(): #handles user inputs
    while True:
        inputWin.clear()
        msg=rawInput(inputWin, 0, 0, winData.username+":")
        if msg != "":
            if msg[0] == "/": #catches commands
                if msg in ["/exit","/quit","/q","/e"]:#command to close the program
                    curses.endwin()
                    globals.kill = True
                elif msg[:7] == "/rename":#command to rename
                    s.sendall(bytes("u"+msg[8:], 'utf-8'))
                    winData.username=msg[8:]
                    updateInfoWin()
            else:#sends everything that isn't a command
                s.sendall(bytes("m"+msg, 'utf-8'))
        if globals.kill == True:
            break
#####################################################
#Start threads
msgOutputThread = threading.Thread(target=msgOutput, args=())
msgOutputThread.start()
msgInputThread = threading.Thread(target=msgInput, args=())
msgInputThread.start()
#####################################################
