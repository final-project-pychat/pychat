"""This is the client code. Run it to connect to a server."""
import socket
import threading
import curses
import time
import json
import random
import math

class RSA:
    def miller_rabin(n, k):
        if n % 5 ==0:
            return False
        r, s = 0, n - 1
        while s % 2 == 0:
            r += 1
            s //= 2
        for _ in range(k):
            a = random.randrange(2, n - 1)
            x = pow(a, s, n)
            if x == 1 or x == n - 1:
                continue
            for _ in range(r - 1):
                x = pow(x, 2, n)
                if x == n - 1:
                    break
            else:
                return False
        return True

    def gen_prime(bit_size):
        number = random.randrange(pow(2,bit_size)+1,pow(2,bit_size+1)-1,2)
        while True:
            if RSA.miller_rabin(number, 40) == True:
                return number
                break
            number = number + 2

    def gen_key(prime_1 = True, prime_2 = True, key_size = 16):
        if prime_1 == True:
            p = RSA.gen_prime(key_size)
        else:
            p = prime_1
        if prime_2 == True:
            q = RSA.gen_prime(key_size)
        else:
            q = prime_2
        N = p * q
        r = (p - 1) * (q - 1)
        candidate_integer = 1
        while True:
            candidate = candidate_integer * r + 1
            n = candidate
            e = 0
            d = 0
            for prime in [3,     7,  11,  13,  17,  19,  23,  29,
                          31,   37,  41,  43,  47,  53,  59,  61,
                          67,   71,  73,  79,  83,  89,  97, 101,
                          103, 107, 109, 113, 127, 131, 137, 139,
                          149, 151, 157, 163, 167, 173, 179, 181,
                          191, 193, 197, 199, 211, 223, 227, 229,
                          233, 239, 241, 251, 257, 263, 269, 271,
                          277, 281, 283, 293, 307, 311, 313, 317, ]:
                if n % prime == 0:
                    e = prime
                    d = n // e
            if d != 0:
                break
            candidate_integer = candidate_integer + 1
        return {"pub_key":{"e":e,"N":N},"priv_key":{"e":e,"d":d,"N":N,"p":p,"q":q,"r":r}}

    def check_key(key):
        if key["pub_key"]["e"] != key["priv_key"]["e"]:
            return False
        checked_num = random.randint(100, pow(10, 10))
        encrypted_num = pow(checked_num, key["pub_key"]["e"], key["pub_key"]["N"])
        decrypted_num = pow(encrypted_num, key["priv_key"]["d"], key["priv_key"]["N"])
        if decrypted_num == checked_num:
            return True
        else:
            return False

    def str_to_int_list(s: str):
        return [ord(x) for x in list(s)]

    def int_list_to_str(int_list: list):
        return_string = ""
        for c in int_list:
            return_string = f"{return_string}{chr(c)}"
        return(return_string)

    def str_to_int(s: str):
        string_bytes = bytes(s, "utf8")
        return(int.from_bytes(string_bytes, "big", signed=False))

    def int_to_str(num: int):
        num_length = max(1, math.ceil(num.bit_length() / 8))
        return(num.to_bytes(num_length, "big").decode("utf8"))

    def encrypt(pub_key, message, return_as_bytes = True, byte_order = "big"):
        message_as_bytes = bytes(message, "utf8")
        message_as_number = int.from_bytes(message_as_bytes, byte_order)
        encrypted_message = pow(message_as_number, pub_key["e"], pub_key["N"])
        if return_as_bytes == True:
            return encrypted_message.to_bytes(int(math.log(encrypted_message, 256))+1, byte_order)
        else:
            return encrypted_message

    def decrypt(priv_key, message, is_bytes = True, byte_order = "big"):
        if is_bytes == True:
            message_as_number = int.from_bytes(message, byte_order)
        else:
            message_as_number = message
        message_number_decrypted = pow(message_as_number, priv_key["d"], priv_key["N"])
        return message_number_decrypted.to_bytes(int(math.log(message_number_decrypted, 256))+1, byte_order).decode()

class Client:  # pylint: disable=too-few-public-methods
    """Class Docstring"""
    def __init__(self):
        self.terminal_1 = Screen()
        self.host = "100.15.255.103"
        self.port = int(self.terminal_1.raw_input("Port:", self.terminal_1.input_window))
        Client.RSA_KEY = RSA.gen_key(key_size = 1028)
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.connect((self.host, self.port))
        #self.has_account = self.terminal_1.raw_input("Do you have an account:", self.terminal_1.input_window)
        #self.username = self.terminal_1.raw_input("Username:", self.terminal_1.input_window)
        self.server_socket.sendall(bytes(json.dumps({
        "message":False,
        "username":False,
        "rsa_key":Client.RSA_KEY["pub_key"]
        }), "utf-8"))
    def start_client(self):
        """Class Docstring"""
        data = self.server_socket.recv(1024).decode("utf8")
        Client.SERVER_KEY = json.loads(data)["rsa_key"]
        while True:
            data = self.server_socket.recv(1024)
            decrypted_data = RSA.decrypt(Client.RSA_KEY["priv_key"], data)
            data_as_dictionary = json.loads(decrypted_data)
            if data_as_dictionary["message"] == "Done":
                #data = json.dumps({
                #"message":False,
                #"username":data_as_dictionary["username"],
                #"clear_terminal":False,
                #"input_prompt":False,
                #"kick":False,
                #"rsa_key":False})
                #encrypted_data = RSA.encrypt(Client.SERVER_KEY, data)
                #self.server_socket.sendall(encrypted_data)
                break
            if data_as_dictionary["message"] != True:
                if data_as_dictionary["input_prompt"] == True:
                    raw_message = self.terminal_1.raw_input(data_as_dictionary["message"], self.terminal_1.input_window)
                    data = json.dumps({
                    "message":raw_message,
                    "username":False,
                    "rsa_key":False
                    })
                    encrypted_data = RSA.encrypt(Client.SERVER_KEY, data)
                    self.server_socket.sendall(encrypted_data)
                else:
                    self.terminal_1.window_print(data_as_dictionary['message'], self.terminal_1.text_window, row=self.terminal_1.text_window.getmaxyx()[0]-1)

        user_input = Input(self.server_socket, self.terminal_1)
        user_output = Output(self.server_socket, self.terminal_1)
        user_input_thread = threading.Thread(target=user_input.start_user_input, args=())
        server_output_thread = threading.Thread(target=user_output.start_server_output, args=())
        user_input_thread.start()
        server_output_thread.start()

class Input:  # pylint: disable=too-few-public-methods
    """Class Docstring"""
    def __init__(self, server_socket, terminal):
        self.server_socket = server_socket
        self.terminal = terminal
    def start_user_input(self):
        """Function Docstring"""
        while True:
            raw_message = self.terminal.raw_input("Message:", self.terminal.input_window)
            data = json.dumps({
            "message":raw_message,
            "username":False,
            "rsa_key":False
            })
            encrypted_data = RSA.encrypt(Client.SERVER_KEY, data)
            self.server_socket.sendall(encrypted_data)

class Output:  # pylint: disable=too-few-public-methods
    """Class Docstring"""
    def __init__(self, server_socket, terminal):
        self.server_socket = server_socket
        self.terminal = terminal
    def start_server_output(self):
        """Function Docstring"""
        while True:
            data = self.server_socket.recv(1024)
            decrypted_data = RSA.decrypt(Client.RSA_KEY["priv_key"], data)
            data_as_dictionary = json.loads(decrypted_data)
            if data_as_dictionary['username'] == False:
                self.terminal.window_print(data_as_dictionary['message'], self.terminal.text_window, row=self.terminal.text_window.getmaxyx()[0]-1)
            else:
                self.terminal.window_print(f"{data_as_dictionary['username']}:{data_as_dictionary['message']}", self.terminal.text_window, row=self.terminal.text_window.getmaxyx()[0]-1)

class Screen:  # pylint: disable=too-few-public-methods
    """Class Docstring"""
    def __init__(self):
        self.terminal = curses.initscr()
        curses.start_color()
        curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_GREEN)
        curses.init_pair(2, curses.COLOR_BLACK, curses.COLOR_YELLOW)
        curses.init_pair(3, 17, 8)
        self.input_window = curses.newwin(2, 0, curses.LINES-2, 0)  # pylint: disable=no-member
        self.text_window = curses.newwin(curses.LINES-3, 0, 1, 0)  # pylint: disable=no-member
        self.info_window = curses.newwin(1, 0, 0, 0)
        self.text_window.bkgd(" ", curses.color_pair(1))
        self.info_window.bkgd(" ", curses.color_pair(3))
        self.input_window.bkgd(" ", curses.color_pair(2))
        self.text_window.scrollok(True)
        self.input_window.refresh()
        self.text_window.refresh()
        self.info_window.refresh()

    def raw_input(self, prompt_string, window, row=0, column=0):  # pylint: disable=no-self-use
        """Allows for input in the specified window. Acts similar to python"s built in input().
        The window needs 2 lines of space to function properly."""
        curses.echo()
        window.addstr(row, column, prompt_string)
        window.refresh()
        user_input = window.getstr(row + 1, column)
        user_input = str(user_input)[2:-1]
        window.clear()
        return user_input

    def window_print(self, string, window, row=0, col=0):  # pylint: disable=no-self-use
        """Function Docstring"""
        window.scroll(1)
        window.addstr(row, col, string)
        window.refresh()

CLIENT_1 = Client()
CLIENT_1.start_client()
