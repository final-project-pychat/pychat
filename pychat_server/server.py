"""
This is the server code. Run it on your server.
"""
import socket
import threading
import sqlite3
import json
import random
import math
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
import time


class RSA:
    def miller_rabin(n, k):
        if n % 5 ==0:
            return False
        r, s = 0, n - 1
        while s % 2 == 0:
            r += 1
            s //= 2
        for _ in range(k):
            a = random.randrange(2, n - 1)
            x = pow(a, s, n)
            if x == 1 or x == n - 1:
                continue
            for _ in range(r - 1):
                x = pow(x, 2, n)
                if x == n - 1:
                    break
            else:
                return False
        return True

    def gen_prime(bit_size):
        number = random.randrange(pow(2,bit_size)+1,pow(2,bit_size+1)-1,2)
        while True:
            if RSA.miller_rabin(number, 40) == True:
                return number
                break
            number = number + 2

    def gen_key(prime_1 = True, prime_2 = True, key_size = 16):
        if prime_1 == True:
            p = RSA.gen_prime(key_size)
        else:
            p = prime_1
        if prime_2 == True:
            q = RSA.gen_prime(key_size)
        else:
            q = prime_2
        N = p * q
        r = (p - 1) * (q - 1)
        candidate_integer = 1
        while True:
            candidate = candidate_integer * r + 1
            n = candidate
            e = 0
            d = 0
            for prime in [3,     7,  11,  13,  17,  19,  23,  29,
                          31,   37,  41,  43,  47,  53,  59,  61,
                          67,   71,  73,  79,  83,  89,  97, 101,
                          103, 107, 109, 113, 127, 131, 137, 139,
                          149, 151, 157, 163, 167, 173, 179, 181,
                          191, 193, 197, 199, 211, 223, 227, 229,
                          233, 239, 241, 251, 257, 263, 269, 271,
                          277, 281, 283, 293, 307, 311, 313, 317, ]:
                if n % prime == 0:
                    e = prime
                    d = n // e
            if d != 0:
                break
            candidate_integer = candidate_integer + 1
        return {"pub_key":{"e":e, "N":N}, "priv_key":{"e":e, "d":d, "N":N, "p":p, "q":q, "r":r}}

    def check_key(key):
        if key["pub_key"]["e"] != key["priv_key"]["e"]:
            return False
        checked_num = random.randint(100, pow(10, 10))
        encrypted_num = pow(checked_num, key["pub_key"]["e"], key["pub_key"]["N"])
        decrypted_num = pow(encrypted_num, key["priv_key"]["d"], key["priv_key"]["N"])
        if decrypted_num == checked_num:
            return True
        else:
            return False

    def str_to_int_list(s: str):
        return [ord(x) for x in list(s)]

    def int_list_to_str(int_list: list):
        return_string = ""
        for c in int_list:
            return_string = f"{return_string}{chr(c)}"
        return(return_string)

    def str_to_int(s: str):
        string_bytes = bytes(s, "utf8")
        return(int.from_bytes(string_bytes, "big", signed=False))

    def int_to_str(num: int):
        num_length = max(1, math.ceil(num.bit_length() / 8))
        return(num.to_bytes(num_length, "big").decode("utf8"))

    def encrypt(pub_key, message, return_as_bytes = True, byte_order = "big"):
        message_as_bytes = bytes(message, "utf8")
        message_as_number = int.from_bytes(message_as_bytes, byte_order)
        encrypted_message = pow(message_as_number, pub_key["e"], pub_key["N"])
        if return_as_bytes == True:
            return encrypted_message.to_bytes(int(math.log(encrypted_message, 256))+1, byte_order)
        else:
            return encrypted_message

    def decrypt(priv_key, message, is_bytes = True, byte_order = "big"):
        if is_bytes == True:
            message_as_number = int.from_bytes(message, byte_order)
        else:
            message_as_number = message
        message_number_decrypted = pow(message_as_number, priv_key["d"], priv_key["N"])
        return message_number_decrypted.to_bytes(int(math.log(message_number_decrypted, 256))+1, byte_order).decode()


class Login_Process:
    email_password = input("Enter password to pychatmailer@gmail.com:")
    def __init__(self, conn, addr, user_key):
        # Key variables
        # Keeping as input because only the server console should have this
        self.code = ""
        self.HasAcc = ""
        self.Uname = ""
        self.Password = ""
        self.Email = ""
        self.user_answer = ""
        self.msg = MIMEMultipart()
        self.msg['From'] = "pychatmailer@gmail.com"
        self.msg['Subject'] = "Verification Code"
        self.s = smtplib.SMTP('smtp.gmail.com: 587')
        self.s.starttls()
        self.s.login(self.msg['From'], Login_Process.email_password)
        self.connection = sqlite3.connect('userdatabase.db')
        self.cursor = self.connection.cursor()
        self.conn = conn
        self.addr = addr
        self.user_key = user_key
        try:
            self.connection.execute("SELECT Username, Password, Email from Users")
        except:
            self.connection.execute('CREATE TABLE "Users" ("Username" TEXT, "Password" TEXT, "Email" TEXT)')
        try:
            self.run()
        except (BrokenPipeError, TimeoutError, ConnectionResetError) as error:
            print(f"Error:{error}\nWhen logging in user:{self.addr}")

    def run(self):
        self.conn.sendall(RSA.encrypt(self.user_key, json.dumps({
        "message":"Welcome to Pychat! Please log into the server.",
        "username":False,
        "clear_terminal":False,
        "input_prompt":False,
        "kick":False,
        "rsa_key":False
        })))
        self.conn.sendall(RSA.encrypt(self.user_key, json.dumps({
        "message":"Do you have an account:",
        "username":False,
        "clear_terminal":False,
        "input_prompt":True,
        "kick":False,
        "rsa_key":False
        })))
        data = self.conn.recv(1024)
        data_as_dictionary = json.loads(RSA.decrypt(Server.RSA_KEY["priv_key"], data))
        self.HasAcc = data_as_dictionary["message"].lower()
        while self.HasAcc not in ["y", "yes", "yep", "n", "no", "nope"]:
            self.conn.sendall(RSA.encrypt(self.user_key, json.dumps({
            "message":f"I don't recognize {self.HasAcc} please try again.",
            "username":False,
            "clear_terminal":False,
            "input_prompt":False,
            "kick":False,
            "rsa_key":False
            })))
            self.conn.sendall(RSA.encrypt(self.user_key, json.dumps({
            "message":"Do you have an account:",
            "username":False,
            "clear_terminal":False,
            "input_prompt":True,
            "kick":False,
            "rsa_key":False
            })))
            data = self.conn.recv(1024)
            data_as_dictionary = json.loads(RSA.decrypt(Server.RSA_KEY["priv_key"], data))
            self.HasAcc = data_as_dictionary["message"].lower()

        if self.HasAcc in ["n", "no", "nope"]:
            Login_Process.make_account(self)

        Login_Process.login_server(self)
        Login_Process.end(self)
        print(self.Uname)

    def email_verification(self):
        # Variable
        self.alphabets_in_capital = []

        # Numbers
        for i in range(48,57):
            self.alphabets_in_capital.append(chr(i))
        # Upercase
        for i in range(65,90):
            self.alphabets_in_capital.append(chr(i))
        # Lowercase
        for i in range(97,122):
            self.alphabets_in_capital.append(chr(i))
        # Picks 8 random numbers/letters
        for i in range(0,8):
            self.code = self.code + str(random.choice(self.alphabets_in_capital))

        # Email's message
        self.msg.attach(MIMEText("This is your code: " + self.code, 'plain'))
        # Sends email
        self.s.sendmail(self.msg['From'], self.msg['To'], self.msg.as_string())

    def enter_info(self):
        self.conn.sendall(RSA.encrypt(self.user_key, json.dumps({
        "message":"Username:",
        "username":False,
        "clear_terminal":False,
        "input_prompt":True,
        "kick":False,
        "rsa_key":False
        })))
        data = self.conn.recv(1024)
        data_as_dictionary = json.loads(RSA.decrypt(Server.RSA_KEY["priv_key"], data))
        self.Uname = data_as_dictionary["message"]
        self.conn.sendall(RSA.encrypt(self.user_key, json.dumps({
        "message":"Email:",
        "username":False,
        "clear_terminal":False,
        "input_prompt":True,
        "kick":False,
        "rsa_key":False
        })))
        data = self.conn.recv(1024)
        data_as_dictionary = json.loads(RSA.decrypt(Server.RSA_KEY["priv_key"], data))
        self.Email = data_as_dictionary["message"]
        self.msg['To'] = data_as_dictionary["message"]
        self.conn.sendall(RSA.encrypt(self.user_key, json.dumps({
        "message":"Password:",
        "username":False,
        "clear_terminal":False,
        "input_prompt":True,
        "kick":False,
        "rsa_key":False
        })))
        data = self.conn.recv(1024)
        data_as_dictionary = json.loads(RSA.decrypt(Server.RSA_KEY["priv_key"], data))
        self.Password = data_as_dictionary["message"]

    # Function to create a new account
    def make_account(self):
        # Gets how big the table is
        self.cursor.execute("SELECT count(*) FROM Users")
        self.table_size = self.cursor.fetchall()

        self.creating = True
        self.passmaking = True

        self.conn.sendall(RSA.encrypt(self.user_key, json.dumps({
        "message":"Please answer the prompts to make your account.",
        "username":False,
        "clear_terminal":False,
        "input_prompt":False,
        "kick":False,
        "rsa_key":False
        })))
        self.enter_info()
        while self.creating == True:
            if self.table_size[0][0] == 0:
                pass
            else:
                self.cursor = self.connection.execute("SELECT Username, Password, Email from Users")

                # Checks if the username or email is already being used
                for row in self.cursor:
                    if self.Uname == row[0] or self.Email == row[2]:
                        self.conn.sendall(RSA.encrypt(self.user_key, json.dumps({
                        "message":f"The username {self.Uname} is already taken or the email {self.Email} is already in use.",
                        "username":False,
                        "clear_terminal":False,
                        "input_prompt":False,
                        "kick":False,
                        "rsa_key":False
                        })))
                        self.enter_info()
            if self.Password not in ["password", "1234567890", "qwertyuiop"] and len(self.Password) >= 8:
                self.creating = False
            else:
                self.conn.sendall(RSA.encrypt(self.user_key, json.dumps({
                "message":"Your password is not secure enough. Please try again.",
                "username":False,
                "clear_terminal":False,
                "input_prompt":False,
                "kick":False,
                "rsa_key":False
                })))
                self.enter_info()
        Login_Process.email_verification(self)
        self.conn.sendall(RSA.encrypt(self.user_key, json.dumps({
        "message":"Please enter the code sent to your email! Enter r to have it sent again:",
        "username":False,
        "clear_terminal":False,
        "input_prompt":True,
        "kick":False,
        "rsa_key":False
        })))
        while True:
            data = self.conn.recv(1024)
            data_as_dictionary = json.loads(RSA.decrypt(Server.RSA_KEY["priv_key"], data))
            self.answer = data_as_dictionary["message"]
            if self.answer == "r":
                self.s.sendmail(self.msg['From'], self.msg['To'], self.msg.as_string())
            elif self.answer == self.code:
                self.conn.sendall(RSA.encrypt(self.user_key, json.dumps({
                "message":"Congratulations! You have successfully created an account.",
                "username":False,
                "clear_terminal":False,
                "input_prompt":False,
                "kick":False,
                "rsa_key":False
                })))
                self.s.quit()
                break
            elif self.answer != self.code:
                self.conn.sendall(RSA.encrypt(self.user_key, json.dumps({
                "message":"Please enter the code sent to your email! Enter r to have it sent again:",
                "username":False,
                "clear_terminal":False,
                "input_prompt":True,
                "kick":False,
                "rsa_key":False
                })))

        self.cursor.execute('INSERT INTO Users (Username, Password, Email) VALUES (?, ?, ?)', (self.Uname, self.Password, self.Email))
        self.connection.commit()
        self.HasAcc = "Y"
        self.reload()

    def reload(self):
        self.cursor = self.connection.execute("SELECT Username, Password, Email from Users")

    def login_server(self):
        self.reload()
        self.Signedin = False
        self.conn.sendall(RSA.encrypt(self.user_key, json.dumps({
        "message":"Please answer the prompts to login.",
        "username":False,
        "clear_terminal":False,
        "input_prompt":False,
        "kick":False,
        "rsa_key":False
        })))
        #time.sleep(3)
        self.enter_info()
        while self.Signedin == False:
            self.accexist = False
            for row in self.cursor:
                print("checking database")
                if (self.Uname, self.Password, self.Email) == (row[0],row[1],row[2]):
                    print("signed in")
                    self.Signedin = True
                    break
                elif self.Uname == row[0] and self.Password != row[1] or self.Email == row[2] and self.Password != row[1]:
                    print("username found")
                    self.accexist = True

            if self.accexist == True:
                print("In accexist")
                self.conn.sendall(RSA.encrypt(self.user_key, json.dumps({
                "message":"Your password is incorrect",
                "username":False,
                "clear_terminal":False,
                "input_prompt":False,
                "kick":False,
                "rsa_key":False})))
                self.enter_info()
            elif self.Signedin == False:
                print("in signedin")
                self.conn.sendall(RSA.encrypt(self.user_key, json.dumps({
                "message":"Login failed",
                "username":False,
                "clear_terminal":False,
                "input_prompt":False,
                "kick":False,
                "rsa_key":False})))
                self.enter_info()


            self.cursor = self.connection.execute("SELECT Username, Password, Email from Users")

    # Closes the connection to the database
    def end(self):
        self.connection.close()
        self.conn.sendall(RSA.encrypt(self.user_key, json.dumps({
        "message":"Successfully Logged In!",
        "username":False,
        "clear_terminal":False,
        "input_prompt":False,
        "kick":False,
        "rsa_key":False})))
        #time.sleep(3)
        self.conn.sendall(RSA.encrypt(self.user_key, json.dumps({
        "message":"Done",
        "username":self.Uname,
        "clear_terminal":False,
        "input_prompt":False,
        "kick":False,
        "rsa_key":False})))


class Server:  # pylint: disable=too-few-public-methods
    """Class Doscstring"""
    USERLIST = []
    def __init__(self):
        self.host = ""
        self.port = int(input("What port:"))
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.bind((self.host, self.port))
        Server.RSA_KEY = RSA.gen_key(key_size = 1028)
        print("server started")
    def start_server(self):
        """Function Docstring"""
        while True:
            try:
                self.server_socket.listen(1)
                conn, addr = self.server_socket.accept()
                print('conected by', addr)
                user = NewConnection(conn, addr)
                user_thread = threading.Thread(target=user.start_connection, args=(), daemon=True)
                user_thread.start()
            except KeyboardInterrupt:
                self.server_socket.close()
                print(f"\nKeyboardInterrupt")
                break

class NewConnection:  # pylint: disable=too-few-public-methods
    """Class Doscstring"""
    def __init__(self, conn, addr):
        self.conn = conn
        self.addr = addr

    def start_connection(self):
        """Function Docstring"""
        username = ""
        user_key = ""
        kill_thread = False
        #self.conn.sendall(bytes(json.dumps(Server.RSA_KEY), "utf8"))
        try:
            self.conn.sendall(bytes(json.dumps({
            "message":False,
            "username":False,
            "clear_terminal":False,
            "input_prompt":False,
            "kick":False,
            "rsa_key":Server.RSA_KEY["pub_key"]
            }), "utf8"))
        except (BrokenPipeError, TimeoutError, ConnectionResetError) as error:
            print(f"Error:{error}\nWhen sending RSA key to new user:{self.addr}")
            kill_thread = True
        data = self.conn.recv(1024).decode("utf8")
        user_key = json.loads(data)["rsa_key"]
        user_login = Login_Process(self.conn, self.addr, user_key)
        print(f"\n\n{user_login}")

        username = json.loads(data)["username"]
        Server.USERLIST.append([self.conn, self.addr, username, user_key])
        print(f"Set {self.addr} username to {username}")
        NewConnection.send_to_userlist(json.dumps({
        "message":f"{username} has connected",
        "username":False,
        "clear_terminal":False,
        "input_prompt":False,
        "kick":False,
        "rsa_key":False
        }))
        while True:
            if kill_thread == True:
                break
            data = self.conn.recv(1024)
            if not data:
                break
            print(data)
            data_as_dictionary = json.loads(RSA.decrypt(Server.RSA_KEY["priv_key"], data))
            print(data_as_dictionary)
            message_data = data_as_dictionary["message"]
            if set(data_as_dictionary) != {'username', 'rsa_key', 'message'}:
                break
            if data_as_dictionary["message"] != False:
                NewConnection.send_to_userlist(json.dumps({
                "message":message_data,
                "username":username,
                "clear_terminal":False,
                "input_prompt":False,
                "kick":False,
                "rsa_key":False
                }))
            if data_as_dictionary["username"] != False:
                Server.USERLIST.remove([self.conn, self.addr, username, user_key])
                username = data_as_dictionary["username"]
                Server.USERLIST.append([self.conn, self.addr, username, user_key])
            if data_as_dictionary["rsa_key"] != False:
                Server.USERLIST.remove([self.conn, self.addr, username, user_key])
                user_key = data_as_dictionary["rsa_key"]
                Server.USERLIST.append([self.conn, self.addr, username, user_key])

    def send_to_userlist(message):
        """Function Docstring"""
        valid_users = []
        invalid_users = []
        for user_info in Server.USERLIST:
            try:
                encrypted_message = RSA.encrypt(user_info[3], message)
                user_info[0].sendall(encrypted_message)
            except (BrokenPipeError, TimeoutError, ConnectionResetError) as error:
                print(f"{user_info[1]} has disconnected.")
                print(f"Caused by {error}")
                print(f"Removing {user_info[1]}.")
                invalid_users.append(user_info)
            else:
                valid_users.append(user_info)

        Server.USERLIST = valid_users

        if invalid_users != []:
            for removed_user in invalid_users:
                #NewConnection.send_to_userlist(f"{removed_user[2]} disconnected. There are {len(Server.USERLIST)} users connected.")
                NewConnection.send_to_userlist(json.dumps({
                "message":f"{removed_user[2]} disconnected. There are {len(Server.USERLIST)} users connected.",
                "username":False,
                "clear_terminal":False,
                "input_prompt":False,
                "kick":False,
                "rsa_key":False
                }))

        #print(message)


SERVER_1 = Server()
SERVER_1.start_server()
